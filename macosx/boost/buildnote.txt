Try these to get the x86 only libs.

./bootstrap.sh --with-libraries=filesystem,program_options,regex,system,thread
#  ./b2 toolset=clang cxxflags="-arch i386 -stdlib=libc++ -std=c++11" linkflags="-arch i386 -stdlib=libc++ -std=c++11" address-model=32 architecture=x86 stage
./b2 toolset=clang cxxflags="-arch i386 -stdlib=libstdc++ -std=gnu++98" linkflags="-arch i386 -stdlib=libstdc++ -std=gnu++98" address-model=32 architecture=x86 stage

# forget installing, just copy from stage/lib
#./b2 install toolset=clang cxxflags="-arch i386 -stdlib=libc++" address-model=32 architecture=x86 stage

