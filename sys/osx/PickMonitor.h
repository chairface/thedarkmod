/*****************************************************************************
                    The Dark Mod GPL Source Code
 
 This file is part of the The Dark Mod Source Code, originally based 
 on the Doom 3 GPL Source Code as published in 2011.
 
 The Dark Mod Source Code is free software: you can redistribute it 
 and/or modify it under the terms of the GNU General Public License as 
 published by the Free Software Foundation, either version 3 of the License, 
 or (at your option) any later version. For details, see LICENSE.TXT.
 
 Project: The Dark Mod (http://www.thedarkmod.com/)
 
 $Revision: 5122 $ (Revision of last commit) 
 $Date: 2011-12-11 11:47:31 -0800 (Sun, 11 Dec 2011) $ (Date of last commit)
 $Author: greebo $ (Author of last commit)
 
******************************************************************************/

#ifndef PICKMONITOR_H
#define PICKMONITOR_H

#import <CoreGraphics/CoreGraphics.h>
#import <AppKit/AppKit.h>

BOOL PickMonitor (CGDirectDisplayID *inOutDisplayID, NSWindow *parentWindow);
BOOL CanUserPickMonitor (void);

#endif // PICKMONITOR_H
